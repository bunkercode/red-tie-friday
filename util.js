/* Harvested from: http://stackoverflow.com/questions/4152931/javascript-inheritance-call-super-constructor-or-use-prototype-chain */
function extend(base, sub) {
    sub.prototype = Object.create(base.prototype);
    Object.defineProperty(sub.prototype, 'constructor', { enumerable: false, value: sub });
}

