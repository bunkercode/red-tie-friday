Red Tie Friday is an application to track gameplay for an Assassin game. Through a web interface it can track player status/score, assign "hits", and deliver death reports. Built with Angular.JS 

--INSTALLATION--
To run the application, simply clone the repo into your preferred webhost's HTML folder. Then browse to index.html

--CONFIGURATION--
Coming soon!

--USAGE--
Coming soon!
