"use strict";

function Player(id) {
    FirebaseBackedObject.call(this, id, "player/");
    this._data.assassins = [];
    Object.seal(this); // Prevent us from using the wrong names for properties
}
extend(FirebaseBackedObject, Player);
Object.defineProperties(Player.prototype, {
    'name': {
        get: function () {
            return this._data.name;
        },
        set: function (newname) {
            this._data.name = newname;
        }
    },
    'assassins': {
        get: function () {
            return this._data.assassins.map(function (id) {
                var a = new Assassin(id);
                a.update();
                return a;
            });
        }
    }
});
Player.prototype.addAssassin = function(assassin) {
    // Add a new assassin to this player
    if (this._data.assassins.indexOf(assassin.id) >= 0) return;
    this._data.assassins.push(assassin.id);
}
Player.prototype.removeAssassin = function(assassin) {
    this._data.assassins = this._data.assassins.filter(function (id) {
        return id != assassin.id;
    });
}
