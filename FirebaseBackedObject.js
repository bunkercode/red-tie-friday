"use strict";

/* FirebaseBackedObject
 *
 * This is the base class that should handle all of the Firebase access entirely,
 * because we want to seperate game logic from database logic.
 *
 * To use this class, you should make a new class for your game object with all
 * of the behavior you need, and make it extend this class.  Make sure to call
 * the base constructor with id and a path for your extended class.
 * */
var firebase_main = "https://redtiefriday.firebaseio.com/main/";
var firebase_test = "https://redtiefriday.firebaseio.com/test/";

function uid() { return new Date().getTime().toString(36); }

var fbroot = firebase_test;
function FirebaseBackedObject(id, fb_path) {
    this.id = id || uid();
    this._data = {};
    this._fb = new Firebase(fbroot + fb_path + this.id);
}
FirebaseBackedObject.prototype.save = function() {
    this._fb.set(this._data);
};
FirebaseBackedObject.prototype.update = function() {
    this._fb.once("value", (function (self) {
        return function(snapshot) { self._data = snapshot.val(); };
    })(this));
};
