"use strict";
function Game(id) {
    FirebaseBackedObject.call(this, id, "game/");
    Object.seal(this); // Prevent us from using the wrong names for properties
}
extend(FirebaseBackedObject, Game);

Object.defineProperties(Game.prototype, {
    "start_time": {
        get: function() { return new Date(this._data.start_time); },
        set: function(date) { this._data.start_time = date.toISOString(); }
    },
    "end_time": {
        get: function() { return new Date(this._data.end_time); },
        set: function(date) { this._data.end_time = date.toISOString(); }
    },
    "is_started": {
        // date comparison will always return false for an invalid date, which is the default
        get: function() { return (this._data.start_time < new Date()); }
    },
    'assassins': {
        get: function () {
            return this._data.assassins.map(function (id) {
                var a = new Assassin(id);
                a.update();
                return a;
            });
        }
    }
});
Game.prototype.addAssassin = function(assassin) {
    // Add a new assassin to this player
    if (typeof(this._data.assassins) === "undefined")
       this._data.assassins = [];
    if (this._data.assassins.indexOf(assassin.id) >= 0)
        return;
    this._data.assassins.push(assassin.id);
}
Game.prototype.removeAssassin = function(assassin) {
    this._data.assassins = this._data.assassins.filter(function (id) {
        return id != assassin.id;
    });
}

Game.prototype.getState = function() {
    // Return "ENDED", "RUNNING", "WAITING", "BROKEN"
    var now = new Date();
    if (this.end_time <= now) 
        return "ENDED";
    if (this.end_time > now && this.start_time < now)
        return "RUNNING";
    if (this.start_time > now) 
        return "WAITING";
    return "BROKEN";
}

/********************************
 * Game logic */
Game.prototype.startGame = function() {
    if (this.is_started) {
        console.warn("Game already started.  Not re-starting game...");
        return;
    }

    this.start_time = new Date();
};
/* Game logic *
********************************/
