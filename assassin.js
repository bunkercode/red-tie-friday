"use strict";

function Assassin(id)  {
    FirebaseBackedObject.call(this, id, "assassin/");
    Object.seal(this); // Prevent us from using the wrong names for properties
}

extend(FirebaseBackedObject, Assassin);

Object.defineProperties(Assassin.prototype, {
    'game': {
        get: function() {
            return new Game(this._data.game);
        },
        set: function(game) {
            this._data.game = game.id;
        }
    },
    'player': {
        get: function() {
            var p = new Player(this._data.player);
            p.update();
            return p;
        },
        set: function(player) {
            this._data.player = player.id;
        }
    },
});
