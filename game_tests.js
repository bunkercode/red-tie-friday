"use strict";

module("game");
test("Game.getState returns WAITING when start_time is in the future", function() {
    var g = new Game();
    var start = new Date();
    start.setHours(start.getHours() + 1); // set start to one hour in the future

    g.start_time = start;

    equal(g.getState(), "WAITING", "game is in WAITING state");
});

test("Game.getState returns RUNING when start_time is in the past and end_time is in the future", function() {
    var g = new Game();
    var start = new Date();
    var end = new Date();
    start.setHours(start.getHours() - 1); // set start to one hour in the past
    end.setHours(end.getHours() + 1); // set end to one hour in the future

    g.start_time = start;
    g.end_time = end;

    equal(g.getState(), "RUNNING", "game is in RUNNING state");
});

test("Game.getState returns ENDED when end_time is in the past", function() {
    var g = new Game();
    var end = new Date();
    end.setHours(end.getHours() - 1); // set end to one hour in the past

    g.end_time = end;

    equal(g.getState(), "ENDED", "game is in ENDED state");
});

test("Game.startGame starts an unstarted game", function() {
    var g = new Game();
    var now = new Date();

    g.startGame();

    equal(g.start_time >= now, true, "game has started since the beginning of the test");
});

test("Game.startGame will not start a game more than once", function() {
    var g = new Game();
    g.startGame();
    var middle_time = new Date();
    g.startGame();
    equal(g.start_time <= middle_time, true, "game start time was not reset when startGame was called again");
});

test("add and remove assassin from game", function() {
    //TODO: How to test removal without including addition? Maybe implement game creation from a data-model, but then I'm testing the constructor instead of the adder.
    //TODO: This test also exercises the assassins getter
    var g = new Game();
    var a = new Assassin();
    g.addAssassin(a);
    equal(g.assassins[0].id, a.id, "The assassin from the game should have the same id as the assassin we made.");
    g.removeAssassin(a);
    equal(g.assassins.length, 0, "The assassin array should now be empty");
});

