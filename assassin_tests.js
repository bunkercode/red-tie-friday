"use strict";

module("assassin");
test("assign game to assassin", function() {
    var a = new Assassin();
    var g = new Game();

    a.game = g;

    equal(a.game.id, g.id, "game ids match");
});

test("assign player to assassin", function() {
    var a = new Assassin();
    var p = new Player();

    a.player = p;

    equal(a.player.id, p.id, "player ids match");
});
