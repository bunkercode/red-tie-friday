"use strict";

var redtie = angular.module('redtie', []);

redtie.controller("LoginCtrl", ["$scope", "$log", "Data", function($scope, $log, Data) {
    $scope.modes = {
        loading: 0,
        playerSelect: 1,
        newPlayer: 2
    }
    $scope.mode = $scope.modes.loading;
    $scope.players = [];
    $scope.selectedPlayer = null;
    $scope.newplayer = {};

    $scope.init = function() {
        Data.getPlayers(function(players) {
            $scope.$apply(function() {
                $scope.players = players;
                $scope.playersLoaded = true;
                $scope.mode = $scope.modes.playerSelect;
            });
        });
    };
    $scope.login = function(p) {
        $log.info($scope.selectedPlayer, p);
    };

    $scope.makeNewPlayer = function() {
        $scope.mode = $scope.modes.newPlayer;
    };

    $scope.newUserSubmit = function() {
        $log.info("new player name is: " + $scope.newplayer.name);
    };
}]);


redtie.service("Data", ["$log", function($log) {
    var fb_root = "https://redtiefriday.firebaseio.com/";
    var fb_game = new Firebase(fb_root).child("game");
    var fb_player = new Firebase(fb_root).child("player");
    var dataService = {
        savePlayer: function(player) {
            fb_player.child(player.id).set(player.getModel());
        },
        loadPlayer: function(id, callback) {
            var player = new Player(id);
            fb_player.child(id).once("value", function(snapshot) {
                player.setModel(snapshot.val());
                if (typeof(callback) === "function") {
                    callback(player);
                }
            });
        },
        getPlayers: function(callback) {
            fb_player.once("value", function(snapshot) {
                var players = [];
                snapshot.forEach(function(eachSnapshot) {
                    // eachSnapshot.val() looks like:    { <player_id>: <player_model> }
                    var player = new Player();
                    player.setModel(eachSnapshot.val());
                    player.id = eachSnapshot.name();
                    players.push(player);
                });
                if (typeof(callback) === "function") {
                    callback(players);
                }
            });
        },
        saveGame: function(game) {
            fb_game.child(game.id).set(game.getModel());
        },
        loadGame: function(id, callback) {
            var game = new Game(id);
            fb_game.child(id).once("value", function(snapshot) {
                game.setModel(snapshot.val());
                if (typeof(callback) === "function") {
                    callback(game);
                }
            });
        },
    };
    return dataService;
}]);
