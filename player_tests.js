"use strict";

module("player");
test("add and remove assassin from player", function() {
    //TODO: How to test removal without including addition? Maybe implement Player creation from a data-model, but then I'm testing the constructor instead of the adder.
    //TODO: This test also exercises the assassins getter
    var p = new Player();
    var a = new Assassin();
    p.addAssassin(a);
    equal(p.assassins[0].id, a.id, "The assassin from the player should have the same id as the assassin we made.");
    p.removeAssassin(a);
    equal(p.assassins.length, 0, "The assassin array should now be empty");
});

test("player has a name attribute", function() {
    var p = new Player();
    var testname = "Darth Vader";

    p.name = testname;

    equal(p.name, testname, "Player name is the name we set");
});
